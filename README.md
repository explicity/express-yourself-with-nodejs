# express-yourself-with-nodejs

A web-server project built with usage of [Node.js](https://nodejs.org/en/) + [Express.js](https://github.com/expressjs/express).

## Overview

The express framework is the most common framework used for developing Node js applications. The express framework is built on top of the node.js framework and helps in fast-tracking development of server based applications.

This server is deployed on [Heroku](https://www.heroku.com/). Heroku is one of the longest-running and popular cloud-based PaaS services.
Links: - https://dashboard.heroku.com/apps/bsa19-nodejs
       - https://bsa19-nodejs.herokuapp.com/ or `heroku open`

## Installation 

`npm install`

`npm run start`

Open http://localhost:3000/

## Basic use

The web-server can handle the list of following requests:

- **GET**: _/user_  
  get an array of all users

- **GET**: _/user/:id_  
  get one user by ID

- **POST**: _/user_  
  create user according to the data from the request body

- **PUT**: _/user/:id_  
  update user according to the data from the request body

- **DELETE**: _/user/:id_  
  delete one user by ID

## Licence 

[MIT](LICENSE)