const express = require("express");
const router = express.Router();

const uuid = require("uuid");
const Joi = require("@hapi/joi");

const { saveName } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

const fighters = require("../views/users/userlist.js");

const validateFighter = fighter => {
  const schema = {
    _id: Joi.string(),
    name: Joi.string().min(2),
    health: Joi.number().integer(),
    attack: Joi.number().integer(),
    defense: Joi.number().integer(),
    source: Joi.string()
  };

  return Joi.validate(fighter, schema);
};

router.get("/", (req, res) => {
  res.json(fighters);
});

router.get("/:id", (req, res) => {
  const found = fighters.some(fighter => fighter._id === req.params.id);

  if (found) {
    res.json(fighters.filter(fighter => fighter._id === req.params.id));
  } else {
    res
      .status(400)
      .json({ msg: `No member with the id of ${req.params.id} was found` });
  }
});

router.post("/", (req, res) => {
  const { error } = validateFighter(req.body);
  if (error) {
    res.status(400).send(error.details[0].message);
    return;  
  }

  const newMember = {
    _id: uuid.v4(),
    name: req.body.name,
    health: req.body.health,
    attack: req.body.attack,
    defense: req.body.defense,
    source: req.body.source
  };

  const { name, health, attack, defense, source } = newMember;

  if (!name || !health || !attack || !defense || !source) {
    res.status(400).json({ msg: "Please fill all the requiered fields." });
  }

  fighters.push(newMember);
  res.json(fighters);

  // const result = saveName(req.body);

  // if (result) {
  //   res.send(`Your name is ${result}`);
  // } else {
  //   res.status(400).send(`Some error`);
  // }
});

router.put("/:id", (req, res) => {
  const found = fighters.some(fighter => fighter._id === req.params.id);

  if (found) {
    const updMember = req.body;

    const { error } = validateFighter(updMember);
    if (error) {
      res.status(400).send(error.details[0].message);
      return;
    }

    fighters.forEach(fighter => {
      let { _id, name, health, attack, defense, source } = updMember;

      if (fighter._id === req.params.id) {
        fighter.name = name ? name : fighter.name;
        fighter.health = health ? health : fighter.health;
        fighter.attack = attack ? attack : fighter.attack;
        fighter.defense = defense ? defense : fighter.defense;
        fighter.source = source ? source : fighter.source;

        res.json({ msg: "Member update", fighter });
      }
    });
  } else {
    res.status(400).json({ msg: "Cannot update" });
  }
});

router.delete("/:id", (req, res) => {
  const found = fighters.find(fighter => fighter._id === req.params.id);

  if (found) {
    const index = fighters.indexOf(found);
    fighters.splice(index, 1);
    res.json({
      msg: "Member deleted",
      fighters: fighters
    });
  } else {
    res
      .status(400)
      .json({ msg: `No member with the id of ${req.params.id} was found` });
  }
});

module.exports = router;
